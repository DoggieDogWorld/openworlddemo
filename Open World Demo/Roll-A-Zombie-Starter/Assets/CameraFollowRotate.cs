﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollowRotate : MonoBehaviour
{

    // store a public reference to the Player game object, so we can refer to it's Transform

    public GameObject player;

    // Store a Vector3 offset from the player (a distance to place the camera from the player at all times)
    private Vector3 offset;

    // At the start of the game..
    void Start()
    {
        // Create an offset by subtracting the Camera's position from the player's position
        offset = transform.position - player.transform.position;
    }

    // After the standard 'Update()' loop runs, and just before each frame is rendered..
    void LateUpdate()
    {
        // Set the position of the Camera (the game object this script is attached to)
        // to the player's position, plus the offset amount
        transform.position = player.transform.position + offset;
    }
    /*
    public GameObject player;
    public float cameraHeight = 20.0f;

    void Update()
    {
        Vector3 pos = player.transform.position;
        pos.z += cameraHeight;
        pos.x = 0;
        pos.y = 0;
        transform.position = pos;
    }
    
    [SerializeField]
    private Transform target;

    [SerializeField]
    private Vector3 offsetPosition;

    [SerializeField]
    private Space offsetPositionSpace = Space.Self;

    [SerializeField]
    private bool lookAt = true;

    private void LateUpdate()
    {
        Refresh();
    }

    public void Refresh()
    {
        if (target == null)
        {
            Debug.LogWarning("Missing target ref !", this);

            return;
        }

        // compute position
        if (offsetPositionSpace == Space.Self)
        {
            transform.position = target.TransformPoint(offsetPosition);
        }
        else
        {
            transform.position = target.position + offsetPosition;
        }

        // compute rotation
        if (lookAt)
        {
            transform.LookAt(target);
        }
        else
        {
         //  transform.rotation = target.rotation;
        }
    }*/
}

/*
 
     public class CameraFollow : MonoBehaviour {
      public GameObject player;
      public float cameraHeight = 20.0f;
  
      void Update() {
          Vector3 pos = player.transform.position;
          pos.z += cameraHeight;
          transform.position = pos;
      }
  }

*/
