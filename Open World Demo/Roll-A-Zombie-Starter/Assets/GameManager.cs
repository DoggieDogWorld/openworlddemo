﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;
public class GameManager : MonoBehaviour {

    private int selectedPosition = 0;
    public GameObject selectedZombie;
    public List<GameObject> zombies;
    public Vector3 selectedSize;
    public Vector3 defaultSize;

    public Text ScoreText;
    private int score = 0;

    // Use this for initialization
	void Start () {
        SelectZombie(selectedZombie);
        ScoreText.text = "Hit the panels.\n Score: " + score;
	}
	
	// Update is called once per frame
	void Update () {

        if (Input.GetKeyDown("a"))
        {
            if (selectedPosition == 0)
            {
                selectedPosition = 3;
                SelectZombie(zombies[3]);
            }
            else
            {
                selectedPosition--;
                SelectZombie(zombies[selectedPosition--]);
            }
        }
        if (Input.GetKeyDown("d"))
        {
            if (selectedPosition == 3)
            {
                selectedPosition = 0;
                SelectZombie(zombies[0]);
            }
            else
            {
                selectedPosition++;
                SelectZombie(zombies[selectedPosition++]);
            }
        }
        if (Input.GetKeyDown("down"))
        {
            Rigidbody rb = selectedZombie.GetComponent<Rigidbody>();
            rb.AddForce(0, 0, -10, ForceMode.Impulse);
        }
        if (Input.GetKeyDown("up"))
        {
            Rigidbody rb = selectedZombie.GetComponent<Rigidbody>();
            rb.AddForce(0, 0, 10, ForceMode.Impulse);
        }
        if (Input.GetKeyDown("left"))
        {
            Rigidbody rb = selectedZombie.GetComponent<Rigidbody>();
            rb.AddForce(-10, 0, 0, ForceMode.Impulse);
        }
        if (Input.GetKeyDown("right"))
        {
            Rigidbody rb = selectedZombie.GetComponent<Rigidbody>();
            rb.AddForce(10, 0, 0, ForceMode.Impulse);
        }
    }

    void SelectZombie(GameObject newZombie)
    {
        selectedZombie.transform.localScale = defaultSize;
        selectedZombie = newZombie;
        newZombie.transform.localScale = selectedSize;
    }

    public void AddScore()
    {
        score++;
        if (score >= 10)
        {
            ScoreText.text = "You Win";
        }
        else
        {
            ScoreText.text = "Score: " + score;
        }


    }
}
