﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreTileManager : MonoBehaviour {

    public GameManager gameManager;
    public AudioClip impact;
    AudioSource source;
	// Use this for initialization
	void Start () {
        source = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter(Collider other)
    {
        gameManager.AddScore();
        source.PlayOneShot(impact);
    }
}
